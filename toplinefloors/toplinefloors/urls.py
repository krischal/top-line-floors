"""toplinefloors URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from webview import views as webviews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', webviews.home, name='home'),
    path('services/<str:service>/', webviews.services ,name='services'),
    #path('services/<str:services>/<str:service/', webviews.services ,name='services'),
    path('aboutus/', webviews.aboutus ,name= 'aboutus'),
    path('contactus/', webviews.contactus ,name= 'contactus'),
    path('quotation/', webviews.getQuotation ,name= 'getQuotation'),
]
