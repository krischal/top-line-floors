from django import forms
from .models import QuotationModel, ContactusModel

class QuotationForm(forms.Form):
    fullname =forms.CharField(max_length=50, required=True)
    contact_number = forms.CharField(max_length =50, required=True)
    sender_email = forms.EmailField(max_length=70, required=True)
    quotation_for = forms.CharField(max_length =50, required=True)
    area = forms.CharField(max_length =20, required=True)
    inquiry_message = forms.CharField(widget=forms.Textarea, required=True)
    inquiry_recieved = forms.DateField(required=False)
    redirect_url = forms.CharField(max_length = 50, required=False)


class ContactusForm(forms.Form):
    fullname = forms.CharField(max_length=50, required=True)
    phone = forms.CharField(max_length =50, required=True)
    email = forms.EmailField(max_length=70, required=True)
    message = forms.CharField(widget=forms.Textarea, required=True)
    message_recieved = forms.DateField(required=False)

    class meta:
        model = ContactusModel
        fields = [
            'fullname',
            'phone',
            'email',
            'message',
            'message_recieved',
            ]