from django.db import models

# Create your models here.

class QuotationModel(models.Model):
    fullname =models.CharField(max_length=50)
    contact_number = models.CharField(max_length =50)
    sender_email = models.EmailField(max_length=70)
    quotation_for = models.CharField(max_length =50)
    area = models.CharField(max_length =20)
    inquiry_message = models.TextField()
    inquiry_recieved = models.DateField()

class ContactusModel(models.Model):
    fullname =models.CharField(max_length=50)
    phone = models.CharField(max_length =50)
    email = models.EmailField(max_length=70)
    message = models.TextField()
    message_recieved = models.DateField()