from django.shortcuts import render, redirect
from .forms import QuotationForm, ContactusForm
from .models import QuotationModel, ContactusModel
from datetime import datetime, date, timedelta, timezone
from django.contrib import messages
from django.template import RequestContext
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
import json
from django.core import serializers

def send_email(email_type, message, from_email):
    subject, from_email, to = email_type, 'sales@toplinefloors.com.au', 'peter@toplinefloors.com.au'
    text_content = message
    html_content = message
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def send_confirmation(to_email, fullname):
    subject, from_email, to = 'Inquiry Recieved Successfully', 'peter@toplinefloors.com.au', to_email
    message = 'Hi '+fullname+',<br><br>Your inquiry has been recieved successfully. Our service team will contact you as soon as possible.<br><br>From,<br>Topline Floors<br>Email: peter@toplinefloors.com.au<br>Phone: 1300 306 838<br>www.toplinefloors.com.au'
    text_content = message
    html_content = message
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

# Create your views here.

def home(request):
    return render(request,'webview/home.html',{'redirect_url':'/'})

def services(request, service):
    template='webview/service-'+service+'.html'
    return render(request, template, {'redirect_url':('services/'+service)})

def contactus(request):
    if request.method == 'GET':
        return render(request, 'webview/contactus.html', {'redirect_url':'contactus/'})
    
    if request.method == 'POST':
        data = ContactusForm(request.POST)
        if data.is_valid():
            #data.save(commit=False)
            createInquiry = ContactusModel()
            createInquiry.fullname =data.cleaned_data.get('fullname')
            createInquiry.phone =data.cleaned_data.get('phone')
            createInquiry.email =data.cleaned_data.get('email')
            createInquiry.message =data.cleaned_data.get('message')
            createInquiry.message_recieved = datetime.now()
            createInquiry.save()
            mail_message= '<p>From: '+data.cleaned_data.get('fullname')+'<br>Phone: '+data.cleaned_data.get('phone')+'<br>Email: '+data.cleaned_data.get('email')+'<br><br>Message: '+data.cleaned_data.get('message')+'</p>'
            send_email(('New Inquiry #'+str(createInquiry.id)),mail_message, data.cleaned_data.get('email'))
            send_confirmation(data.cleaned_data.get('email'), data.cleaned_data.get('fullname'))
            messages.success(request, 'Your inquiry has successfully been recieved. Our service team will contact you within 30 Minutes.')
            return render(request, 'webview/contactus.html')

        messages.success(request, 'Error occured while sending inquiry.')
        return render(request, 'webview/contactus.html', {'redirect_url':'contactus/'})
            

def aboutus(request):
    return render(request, 'webview/aboutus.html', {'redirect_url':'aboutus/'})

def getQuotation(request):
    if request.method == 'POST':
        data = QuotationForm(request.POST)
        if data.is_valid():
            setQuote = QuotationModel()
            setQuote.fullname =data.cleaned_data.get('fullname')
            setQuote.contact_number =data.cleaned_data.get('contact_number')
            setQuote.sender_email =data.cleaned_data.get('sender_email')
            setQuote.quotation_for =data.cleaned_data.get('quotation_for')
            setQuote.area = data.cleaned_data.get('area')
            setQuote.inquiry_message =data.cleaned_data.get('inquiry_message')
            setQuote.inquiry_recieved = datetime.now()
            try:
                setQuote.save()
                mail_message= '<p>From: '+data.cleaned_data.get('fullname')+'<br>Phone: '+data.cleaned_data.get('contact_number')+'<br>Email: '+data.cleaned_data.get('sender_email')+'<br>Quotation For: '+data.cleaned_data.get('quotation_for')+'<br>Area: '+data.cleaned_data.get('area')+' m.sq'+'<br><br>Message: '+data.cleaned_data.get('inquiry_message')+'</p>'
                send_email(('#'+str(setQuote.id))+' Quotation Request for '+data.cleaned_data.get('quotation_for'),mail_message, data.cleaned_data.get('sender_email'))
                send_confirmation(data.cleaned_data.get('sender_email'), data.cleaned_data.get('fullname'))
                messages.success(request, 'Your Quotation request has successfully been recieved. Our service team will contact you within 30 Minutes.')
                return redirect(home)
            except Exception as e:
                return redirect(home)

    messages.error(request, 'Error occured while requesting for Quotation.')
    return redirect(home)


