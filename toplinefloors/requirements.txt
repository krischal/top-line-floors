Django==3.0.5
django-allauth==0.41.0
django-widget-tweaks==1.4.8
oauthlib==3.1.0
python3-openid==3.1.0
qtconsole==4.7.3
QtPy==1.9.0
requests==2.23.0
requests-oauthlib==1.3.0
sqlparse==0.3.1
urllib3==1.25.9
gunicorn

